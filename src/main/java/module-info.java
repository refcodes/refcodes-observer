module org.refcodes.observer {
	requires org.refcodes.generator;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.matcher;
	requires transitive org.refcodes.mixin;
	requires java.logging;

	exports org.refcodes.observer;
}
