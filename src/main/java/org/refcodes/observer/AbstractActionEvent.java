// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

/**
 * The {@link AbstractActionEvent} provides a base implementation for an
 * {@link ActionEvent}.
 *
 * @param <A> The generic type of the event's action in question.
 * @param <SRC> The generic type of the event's source in question.
 */
public abstract class AbstractActionEvent<A, SRC> extends AbstractEvent<SRC> implements ActionEvent<A, SRC> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected A _action;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an event with the given source.
	 *
	 * @param aSource The source from which this event originated.
	 */
	public AbstractActionEvent( SRC aSource ) {
		super( aSource );
		_action = null;
	}

	/**
	 * Constructs an event with the given source.
	 *
	 * @param aAction The action which the {@link ActionEvent} represents.
	 * @param aSource The source from which this event originated.
	 */
	public AbstractActionEvent( A aAction, SRC aSource ) {
		super( aSource );
		_action = aAction;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public A getAction() {
		return _action;
	}
}
