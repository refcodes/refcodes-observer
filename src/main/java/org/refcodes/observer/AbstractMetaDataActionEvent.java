// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

/**
 * The {@link AbstractMetaDataActionEvent} provides a base implementation for an
 * {@link MetaDataActionEvent}.
 *
 * In order to distinguish {@link AbstractMetaDataActionEvent} instances from
 * each other, you may create an actions enumeration, enumerating the various
 * event actions you support. Pass the actual action you intend to notify upon
 * to the according constructor, as an {@link Observer} you may use the
 * declarative method {@link EventMatcherSugar#actionEqualWith(Object)} to test
 * whether your action was notified (or a simple switch case statement).
 * 
 * @param <A> The type of the action stored in the event.
 * @param <EM> The type of the EventMetaData
 * @param <SRC> The type of the source in question.
 */
public abstract class AbstractMetaDataActionEvent<A, EM extends EventMetaData, SRC> extends AbstractActionEvent<A, SRC> implements MetaDataActionEvent<A, EM, SRC> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected EM _eventMetaData;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	public AbstractMetaDataActionEvent( EM aEventMetaData, SRC aSource ) {
		super( aSource );
		_eventMetaData = aEventMetaData;
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	public AbstractMetaDataActionEvent( A aAction, EM aEventMetaData, SRC aSource ) {
		super( aAction, aSource );
		_eventMetaData = aEventMetaData;
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aAction The action which the event represents.
	 * @param aSource The source from which this event originated.
	 */
	public AbstractMetaDataActionEvent( A aAction, SRC aSource ) {
		super( aAction, aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aSource The source from which this event originated.
	 */
	public AbstractMetaDataActionEvent( SRC aSource ) {
		super( aSource );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EM getMetaData() {
		return _eventMetaData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [eventMetaData=" + _eventMetaData + ", action=" + getAction() + "]";
	}
}
