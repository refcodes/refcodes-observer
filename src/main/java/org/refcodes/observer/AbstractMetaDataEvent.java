// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

/**
 * The {@link AbstractMetaDataEvent} provides a base implementation for an
 * {@link MetaDataEvent}.
 * 
 * @param <EM> The type of the EventMetaData
 * @param <SRC> The type of the source in question.
 */
public abstract class AbstractMetaDataEvent<EM extends EventMetaData, SRC> extends AbstractEvent<SRC> implements MetaDataEvent<EM, SRC> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected EM _eventMetaData;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	public AbstractMetaDataEvent( EM aEventMetaData, SRC aSource ) {
		super( aSource );
		_eventMetaData = aEventMetaData;
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param channel The value for {@link EventMetaData#getChannel()}
	 *        attribute.
	 * @param aSource The source from which this event originated.
	 */
	public AbstractMetaDataEvent( String channel, SRC aSource ) {
		super( aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aSource The source from which this event originated.
	 */
	public AbstractMetaDataEvent( SRC aSource ) {
		super( aSource );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EM getMetaData() {
		return _eventMetaData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "MetaDataEvent [eventMetaData=" + _eventMetaData + "]";
	}
}
