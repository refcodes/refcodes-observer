// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

import org.refcodes.mixin.ActionAccessor;

/**
 * An {@link ActionEvent} extends the {@link Event} and provides additional
 * means to provide an action via {@link #getAction()} being of a generic type.
 * The action can be used to easily describe the intend of the event without
 * sub-classing an {@link ActionEvent} enabled {GenericEvent} class for each
 * intend.
 * 
 * @param <A> The type of the action stored in the event.
 * @param <SRC> The type of the source in question.
 */
public interface ActionEvent<A, SRC> extends Event<SRC>, ActionAccessor<A> {}
