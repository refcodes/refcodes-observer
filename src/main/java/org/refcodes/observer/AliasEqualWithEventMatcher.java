// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.observer;

import org.refcodes.matcher.MatcherSchema;
import org.refcodes.schema.Schema;

/**
 * Matches the given alias with the alias stored in an event's meta data (ALIAS
 * EQUAL WITH).
 * 
 * @param <E> The matchee type.
 */
public class AliasEqualWithEventMatcher<E extends MetaDataEvent<?, ?>> extends AbstractEventMatcher<E> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "ALIAS_EQUAL_WITH";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _alias;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new alias equal with matcher impl.
	 *
	 * @param aAlias the alias
	 */
	public AliasEqualWithEventMatcher( String aAlias ) {
		super( ALIAS, "Matches the given alias <" + aAlias + "> with the alias stored in an event's meta data (ALIAS EQUAL WITH)." );
		_alias = aAlias;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( E aEvent ) {
		if ( _alias != null ) {
			return ( _alias.equals( aEvent.getMetaData().getAlias() ) );
		}
		else if ( aEvent.getMetaData().getAlias() == null ) {
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatcherSchema toSchema() {
		final MatcherSchema theSchema = super.toSchema();
		theSchema.put( Schema.VALUE, _alias );
		return theSchema;
	}
}