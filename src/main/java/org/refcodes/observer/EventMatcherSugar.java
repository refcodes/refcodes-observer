// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

import org.refcodes.matcher.Matcher;
import org.refcodes.matcher.MatcherSchema;
import org.refcodes.matcher.MatcherSugar;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the {@link EventMatcher} elements.
 */
public class EventMatcherSugar {

	private EventMatcherSugar() {
		throw new IllegalStateException( "Utility class" );
	}

	/**
	 * Catches all events, no matching is done.
	 *
	 * @param <E> The type of the event to be matched
	 * 
	 * @return The "catch-all" {@link EventMatcher}.
	 */
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> catchAll() {
		return new CatchAllEventMatcher<>();
	}

	/**
	 * Catches no event, no matching is done.
	 *
	 * @param <E> The type of the event to be matched
	 * 
	 * @return The "catch-none" {@link EventMatcher}.
	 */
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> catchNone() {
		return new CatchNoneEventMatcher<>();
	}

	/**
	 * Factory method to create an event matcher by event type.
	 * 
	 * @param <E> The type of the event to be matched
	 * @param aEventType The event type to be matched by this matcher.
	 * 
	 * @return An event matcher by event type.
	 */
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> isAssignableFrom( Class<?> aEventType ) {
		return new EventMatcherWrapper<>( MatcherSugar.isAssignableFrom( aEventType ) );
	}

	/**
	 * Factory method to create an event matcher by event publisher type.
	 * 
	 * @param <E> The type of the event to be matched
	 * @param <PT> The publisher descriptor type
	 * @param aPublisherType The event publisher type to be matched by this
	 *        matcher.
	 * 
	 * @return An event matcher by event type.
	 */
	public static <E extends MetaDataEvent<?, ?>, PT extends Object> EventMatcher<E> publisherIsAssignableFrom( Class<? extends PT> aPublisherType ) {
		return new PublisherTypeOfEventMatcher<E, PT>( aPublisherType );
	}

	/**
	 * Factory method to create an "OR" matcher for the given matchers.
	 * 
	 * @param <E> The event type applied to the matcher
	 * @param aEventMatchers The matchers to be combined by an "OR".
	 * 
	 * @return An "OR" matcher.
	 */
	@SafeVarargs
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> or( EventMatcher<E>... aEventMatchers ) {
		return new EventMatcherWrapper<>( MatcherSugar.or( aEventMatchers ) );
	}

	/**
	 * Factory method to create an "AND" matcher for the given matchers.
	 * 
	 * @param <E> The event type applied to the matcher
	 * @param aEventMatchers The matchers to be combined by an "AND".
	 * 
	 * @return An "AND" matcher.
	 */
	@SafeVarargs
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> and( EventMatcher<E>... aEventMatchers ) {
		return new EventMatcherWrapper<>( MatcherSugar.and( aEventMatchers ) );
	}

	/**
	 * Factory method to create an "ALIAS EQUAL WITH" matcher for the given
	 * alias compared with the alias stored in the {@link EventMetaData}.
	 * 
	 * @param <E> The event type applied to the matcher
	 * @param aAlias The alias to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s name property.
	 * 
	 * @return An "ALIAS EQUAL WITH" matcher regarding the
	 *         {@link MetaDataEvent}'s name property.
	 */
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> aliasEqualWith( String aAlias ) {
		return new AliasEqualWithEventMatcher<>( aAlias );
	}

	/**
	 * Factory method to create an "GROUP EQUAL WITH" matcher for the given
	 * group compared with the group stored in the {@link EventMetaData}.
	 * 
	 * @param <E> The event type applied to the matcher
	 * @param aGroup The group to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s group property.
	 * 
	 * @return An "GROUP EQUAL WITH" matcher regarding the
	 *         {@link MetaDataEvent}'s group property.
	 */
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> groupEqualWith( String aGroup ) {
		return new GroupEqualWithEventMatcher<>( aGroup );
	}

	/**
	 * Factory method to create an "CHANNEL EQUAL WITH" matcher for the given
	 * channel compared with the channel stored in the {@link EventMetaData}.
	 * 
	 * @param <E> The event type applied to the matcher
	 * @param aChannel The channel to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s channel property.
	 * 
	 * @return An "CHANNEL EQUAL WITH" matcher regarding the
	 *         {@link MetaDataEvent}'s channel property.
	 */
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> channelEqualWith( String aChannel ) {
		return new ChannelEqualWithEventMatcher<>( aChannel );
	}

	/**
	 * Factory method to create an "UNIVERSAL ID EQUAL WITH" matcher for the
	 * given UID compared with the UID stored in the {@link EventMetaData}.
	 * 
	 * @param <E> The event type applied to the matcher
	 * @param aUid The UID to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s UID property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s UID
	 *         property.
	 */
	public static <E extends MetaDataEvent<?, ?>> EventMatcher<E> universalIdEqualWith( String aUid ) {
		return new UniversalIdEqualWithEventMatcher<>( aUid );
	}

	/**
	 * Factory method to create an "ACTION EQUAL WITH" matcher for the given
	 * action compared with the action stored in the {@link EventMetaData}.
	 * 
	 * @param <E> The event type applied to the matcher.
	 * @param <A> The type of the action stored in the event. CAUTION: The
	 *        drawback of not using generic generic type declaration on a class
	 *        level is no granted type safety, the advantage is the ease of use:
	 *        Sub-classes can be used out of the box.
	 * @param aAction The action to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s action property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link ActionEvent}'s
	 *         action property.
	 */
	public static <E extends MetaDataActionEvent<A, ?, ?>, A> EventMatcher<E> actionEqualWith( A aAction ) {
		return new ActionEqualWithEventMatcher<>( aAction );
	}

	//-------------------------------------------------------------------------
	// INNER CLASSES:

	static class EventMatcherWrapper<E extends MetaDataEvent<?, ?>> implements EventMatcher<E> {

		private final Matcher<E> _eventMatcher;

		/**
		 * Instantiates a new event matcher wrapper.
		 *
		 * @param aMatcher the matcher
		 */
		public EventMatcherWrapper( Matcher<E> aMatcher ) {
			assert ( aMatcher != null );
			_eventMatcher = aMatcher;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isMatching( E aEvent ) {
			return _eventMatcher.isMatching( aEvent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public MatcherSchema toSchema() {
			return new MatcherSchema( getClass(), _eventMatcher.toSchema() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getAlias() {
			return _eventMatcher.getAlias();
		}
	}
}
