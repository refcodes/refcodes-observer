// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

import java.util.UUID;

import org.refcodes.mixin.AliasAccessor;
import org.refcodes.mixin.ChannelAccessor;
import org.refcodes.mixin.GroupAccessor;
import org.refcodes.mixin.MetaDataAccessor.MetaDataBuilder;
import org.refcodes.mixin.UniversalIdAccessor;

/**
 * The Meta-Data describes the event which a publisher posts via the event bus
 * to an event subscriber's event listener. The Meta-Data is the criteria most
 * commonly used by an event matcher in order to identify an loosely coupled
 * event.
 */
public class EventMetaData implements UniversalIdAccessor, AliasAccessor, GroupAccessor, ChannelAccessor, PublisherTypeAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _alias;
	protected String _channel;
	protected String _group;
	protected String _uid;
	protected Class<?> _publisherType;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private EventMetaData( Builder builder ) {
		_alias = builder.alias;
		_channel = builder.channel;
		_group = builder.group;
		_uid = builder.uid;
		_publisherType = builder.publisherType;
	}

	/**
	 * Constructs the {@link EventMetaData} with no properties set except the
	 * Universal-TID.
	 */
	public EventMetaData() {
		_uid = UUID.randomUUID().toString();
	}

	/**
	 * Constructs the {@link EventMetaData} with the given properties.
	 * 
	 * @param aPublisherType The type of the publisher.
	 */
	public EventMetaData( Class<?> aPublisherType ) {
		_uid = UUID.randomUUID().toString();
		_publisherType = aPublisherType;
	}

	/**
	 * Constructs the {@link EventMetaData} with the properties provided by the
	 * given {@link EventMetaData} instance.
	 * 
	 * @param aMetaData The {@link EventMetaData} instance from which to get the
	 *        properties.
	 */
	public EventMetaData( EventMetaData aMetaData ) {
		_alias = aMetaData.getAlias();
		_group = aMetaData.getGroup();
		_channel = aMetaData.getChannel();
		_uid = aMetaData.getUniversalId();
		_publisherType = aMetaData.getPublisherType();
	}

	/**
	 * Constructs the {@link EventMetaData} with the given properties.
	 * 
	 * @param aChannel The channel for the {@link EventMetaData}.
	 */
	public EventMetaData( String aChannel ) {
		_channel = aChannel;
		_uid = UUID.randomUUID().toString();
	}

	/**
	 * Constructs the {@link EventMetaData} with the given properties.
	 * 
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 */
	public EventMetaData( String aChannel, Class<?> aPublisherType ) {
		_channel = aChannel;
		_publisherType = aPublisherType;
		_uid = UUID.randomUUID().toString();
	}

	/**
	 * Constructs the {@link EventMetaData} with the given properties.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 */
	public EventMetaData( String aAlias, String aGroup ) {
		_alias = aAlias;
		_group = aGroup;
		_uid = UUID.randomUUID().toString();
	}

	/**
	 * Constructs the {@link EventMetaData} with the given properties.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 */
	public EventMetaData( String aAlias, String aGroup, String aChannel ) {
		_alias = aAlias;
		_group = aGroup;
		_channel = aChannel;
		_uid = UUID.randomUUID().toString();
	}

	/**
	 * Constructs the {@link EventMetaData} with the given properties.
	 *
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 */
	public EventMetaData( String aAlias, String aGroup, String aChannel, Class<?> aPublisherType ) {
		_alias = aAlias;
		_group = aGroup;
		_channel = aChannel;
		_publisherType = aPublisherType;
		_uid = UUID.randomUUID().toString();
	}

	/**
	 * Constructs the {@link EventMetaData} with the given properties.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aUid The Universal-TID for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 */
	public EventMetaData( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		_alias = aAlias;
		_group = aGroup;
		_channel = aChannel;
		_uid = aUid;
		_publisherType = aPublisherType;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAlias() {
		return _alias;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getChannel() {
		return _channel;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getGroup() {
		return _group;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?> getPublisherType() {
		return _publisherType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUniversalId() {
		return _uid;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [UID=" + _uid + ", alias=" + _alias + ", group=" + _group + ", channel=" + _channel + ", publisherType=" + ( _publisherType != null ? _publisherType.getSimpleName() : null ) + "]";
	}

	/**
	 * Creates builder to build {@link EventMetaData}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link EventMetaData}.
	 */
	public static final class Builder implements AliasBuilder<Builder>, ChannelBuilder<Builder>, GroupBuilder<Builder>, UniversalIdBuilder<Builder>, PublisherTypeBuilder<Builder>, MetaDataBuilder<EventMetaData, Builder> {

		private String alias;
		private String channel;
		private String group;
		private String uid;
		private Class<?> publisherType;

		private Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAlias( String aAlias ) {
			alias = aAlias;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withChannel( String aChannel ) {
			channel = aChannel;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withGroup( String aGroup ) {
			group = aGroup;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withUniversalId( String aUid ) {
			uid = aUid;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPublisherType( Class<?> aPublisherType ) {
			publisherType = aPublisherType;
			return this;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * Merges all not-null values of the provided {@link EventMetaData}
		 * instance into this {@link Builder} instance.
		 * 
		 * @param aEventMetaData The {@link EventMetaData} instance to be merged
		 *        into this {@link Builder} instance.
		 * 
		 * @return This {@link Builder} instance as of the builder pattern.
		 */
		@Override
		public Builder withMetaData( EventMetaData aEventMetaData ) {
			if ( aEventMetaData.getAlias() != null ) {
				alias = aEventMetaData.getAlias();
			}
			if ( aEventMetaData.getChannel() != null ) {
				channel = aEventMetaData.getChannel();
			}
			if ( aEventMetaData.getGroup() != null ) {
				group = aEventMetaData.getGroup();
			}
			if ( aEventMetaData.getPublisherType() != null ) {
				publisherType = aEventMetaData.getPublisherType();
			}
			if ( aEventMetaData.getUniversalId() != null ) {
				uid = aEventMetaData.getUniversalId();
			}
			return this;
		}

		/**
		 * Builder method of the builder.
		 * 
		 * @return The built instance.
		 */
		public EventMetaData build() {
			return new EventMetaData( this );
		}
	}
}
