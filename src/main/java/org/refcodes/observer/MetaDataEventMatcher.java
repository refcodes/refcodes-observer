// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

import org.refcodes.matcher.Matcher;

/**
 * This interface typifies the {@link Matcher} interface for the usage with
 * {@link MetaDataEvent} instances. An {@link EventMatcher} most commonly uses
 * the {@link EventMetaData} (as defined with the {@link MetaDataEvent}) in
 * order to determine whether an {@link ActionEvent} matches
 * {@link EventMetaData} properties or not.
 *
 * @param <E> The matchee type
 */
public interface MetaDataEventMatcher<E extends MetaDataEvent<?, ?>> extends EventMatcher<E> {

	/**
	 * Tests whether the given event is matching the mathcer's criteria.
	 * 
	 * @param aEvent The event used for testing its matchability.
	 * 
	 * @return True in case the event matches the matcher's criteria, else
	 *         false.
	 */
	@Override
	boolean isMatching( E aEvent );
}
