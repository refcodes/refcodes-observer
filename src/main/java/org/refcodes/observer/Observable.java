// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

/**
 * The {@link Observable} interface can be implemented by any class which can be
 * observed by {@link Observer} instances (proposal) or any listener interface
 * (custom as of your needs). Whenever something interesting happens to the
 * {@link Observable}, then the listeners are informed according to the
 * {@link Observable}'s implementation. Please regard the listener's
 * documentation on the behavior of the actual listener's methods. Extends it
 * with further functionality using the {@link Observers} interface.
 *
 * @param <O> The observer type (event listener) observing the {@link Observer}.
 */
public interface Observable<O> {

	/**
	 * Tests whether the given observer (event listener) has been added to this
	 * {@link Observable}.
	 * 
	 * @param aObserver The observer (event listener) for which to test if it
	 *        has been added.
	 * 
	 * @return True if the given observer (event listener) has been added
	 *         already.
	 */
	boolean hasObserver( O aObserver );

	/**
	 * Adds the given observer (event listener). The observer (event listener)
	 * itself acts as the handle which is used when removing the given observer
	 * (event listener) later.
	 * 
	 * @param aObserver The observer (event listener) which is to be added to
	 *        the {@link Observable}.
	 * 
	 * @return True if the observer (event listener) has been added
	 *         successfully. If the observer (event listener) has already been
	 *         added, false is returned.
	 */
	boolean subscribeObserver( O aObserver );

	/**
	 * Removes the observer (event listener). In case the observer (event
	 * listener) has not been added before, then false is returned.
	 * 
	 * @param aObserver The observer (event listener) which is to be removed.
	 * 
	 * @return True if the observer (event listener) has been removed
	 *         successfully. If there was none such observer (event listener) or
	 *         if the observer (event listener) has already been removed, then
	 *         false is returned.
	 */
	boolean unsubscribeObserver( O aObserver );
}
