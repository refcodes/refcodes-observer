package org.refcodes.observer;

/**
 * An {@link ObservableObserver} is an observer, observing an observable. In
 * this case, it is a listener interface listening for the most basic
 * functionality an observable must provide in order to be an observable:
 * Subscribing and unsubscribing of listeners (observers). The implementing
 * observer, when registered to an according observable, gets its
 * {@link #onSubscribe(SubscribeEvent)} or
 * {@link #onUnsubscribe(UnsubscribeEvent)} methods invoked when the according
 * functionality is invoked upon the observable in question.
 * 
 * @param <O> The type of the observable being observed.
 */
public interface ObservableObserver<O extends Observable<?>> {

	/**
	 * This method is called when information about an Observable which was
	 * previously requested using an asynchronous interface becomes available.
	 *
	 * @param aSubscribeEvent the subscribe event
	 */
	default void onSubscribe( SubscribeEvent<O> aSubscribeEvent ) {}

	/**
	 * This method is called when information about an Observable which was
	 * previously requested using an asynchronous interface becomes available.
	 *
	 * @param aUnsubscribeEvent the unsubscribe event
	 */
	default void onUnsubscribe( UnsubscribeEvent<O> aUnsubscribeEvent ) {}
}
