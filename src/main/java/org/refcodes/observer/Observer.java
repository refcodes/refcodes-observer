// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

/**
 * The listener of an event subscriber to be fed with events by a event
 * publisher.
 *
 * @param <E> the element type
 */
@FunctionalInterface
public interface Observer<E extends Event<?>> {

	/**
	 * The listener implementing this interface is notified of an event via this
	 * method. The publisher pushes the event to the subscriber.
	 * 
	 * @param aEvent aEvent The event to be pushed from the publisher to the
	 *        subscriber.
	 */
	void onEvent( E aEvent );
}
