// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

/**
 * This {@link ObserverDescriptor} describes the {@link Observer}, i.e. in some
 * cases an {@link Observer} requires an {@link EventMatcher} which determines
 * which {@link ActionEvent} instances to pass to the {@link Observer}. The
 * {@link ObserverDescriptor} is itself an {@link Observer} calling the
 * {@link EventMatcher} to determine whether to delegate the {@link ActionEvent}
 * to contained {@link Observer} or not.
 *
 * @param <E> the element type
 * @param <O> the generic type
 * @param <EM> the generic type
 */
public class ObserverDescriptor<E extends Event<?>, O extends Observer<E>, EM extends EventMatcher<E>> implements Observer<E> {

	private final O _observer;
	private final EM _eventMatcher;

	/**
	 * Constructs the event listener descriptor with the given event listener
	 * and the given event matcher.
	 * 
	 * @param aEventListener The listener to be stored in the event listener
	 *        descriptor.
	 * @param aEventMatcher The matcher to be stored in the event listener
	 *        descriptor.
	 */
	public ObserverDescriptor( O aEventListener, EM aEventMatcher ) {
		_observer = aEventListener;
		_eventMatcher = aEventMatcher;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onEvent( E aEvent ) {
		if ( _eventMatcher == null || _eventMatcher.isMatching( aEvent ) ) {
			_observer.onEvent( aEvent );
		}
	}

	/**
	 * Returns the {@link Observer} associated with the given
	 * {@link EventMatcher}.
	 *
	 * @return The {@link Observer} as it is being managed by the
	 *         {@link ObserverDescriptor}.
	 */
	public O getObserver() {
		return _observer;
	}

	/**
	 * Returns the {@link EventMatcher} associated with the given
	 * {@link Observer}.
	 * 
	 * @return The {@link EventMatcher} as it is being managed by the
	 *         {@link ObserverDescriptor}.
	 */
	public EM getEventMatcher() {
		return _eventMatcher;
	}
}
