// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

import java.util.Iterator;

/**
 * Provides methods to manage all subscribed {@link Observer} instances as a
 * whole and is intended as an extension of the {@link Observable} interface.
 *
 * @param <O> the generic type
 * @param <B> The builder to return in order to be able to apply multiple build
 *        operations.
 */
public interface Observers<O, B extends Observers<O, B>> {

	/**
	 * Enables or disables all {@link Observer} instances.
	 * 
	 * @param isActive When true then Request-Correlation is enabled, else
	 *        disabled.
	 */
	void setObserversActive( boolean isActive );

	/**
	 * Builder method for the {@link #setObserversActive(boolean)} operation.
	 * 
	 * @param isActive When true then Request-Correlation is enabled, else
	 *        disabled.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	B withObserversActive( boolean isActive );

	/**
	 * Enables all observables.
	 */
	default void enableObservers() {
		setObserversActive( true );
	}

	/**
	 * Builder method for the {@link #enableObservers} operation.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	B withEnableObservers();

	/**
	 * Disables all observables.
	 */
	default void disableObservers() {
		setObserversActive( false );
	}

	/**
	 * Returns true when all {@link Observer} instances are theoretically active
	 * (the instance may also decide by itself), else false.
	 * 
	 * @return True when active, false when inactive (disabled).
	 */
	boolean isObserversActive();

	/**
	 * Builder method for the {@link #disableObservers} operation.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	B withDisableObservers();

	/**
	 * Returns an {@link Iterator} with the registered observers.
	 * 
	 * @return The according {@link Iterator}.
	 */
	Iterator<O> observers();
}
