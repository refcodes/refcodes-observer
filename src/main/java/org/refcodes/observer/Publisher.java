// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

/**
 * A system publishing {@link ActionEvent} instances (such as an event bus), may
 * implement this interface. An observable (event listener), in case of being of
 * type {@link Observer}, may be notified using the
 * {@link Observer#onEvent(Event)} method.
 *
 * @param <E> the element type
 */
public interface Publisher<E extends Event<?>> {

	/**
	 * The listener implementing this interface is notified of an event via this
	 * method. The publisher pushes the event to the subscriber. When this
	 * method exits, then all listeners have been notified with the given event.
	 * I.e. even if each event is dispatched in an own thread, then the longest
	 * running thread determines when this method finishes.
	 * 
	 * @param aEvent aEvent The event to be pushed from the publisher to the
	 *        subscriber.
	 */
	void publishEvent( E aEvent );
}
