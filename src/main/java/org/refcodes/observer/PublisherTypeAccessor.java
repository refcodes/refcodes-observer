// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

/**
 * Provides an accessor for a publisher's type property.
 */
public interface PublisherTypeAccessor {

	/**
	 * Retrieves the publisher's type property.
	 * 
	 * @return The publisher's type stored by the property.
	 */
	Class<?> getPublisherType();

	/**
	 * Provides a mutator for a publisher's type property.
	 * 
	 */
	public interface PublisherTypeMutator {

		/**
		 * Sets the publisherType for the property.
		 * 
		 * @param aPublisherType The publisher's type to be stored by the
		 *        property.
		 */
		void setPublisherType( Class<?> aPublisherType );
	}

	/**
	 * Provides a builder method for a publisher's type property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface PublisherTypeBuilder<B extends PublisherTypeBuilder<B>> {

		/**
		 * Sets the publisherType for the publisher's type property.
		 * 
		 * @param aPublisherType The publisher's type to be stored by the
		 *        publisher's type property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withPublisherType( Class<?> aPublisherType );
	}

	/**
	 * Provides a publisher's type property.
	 */
	public interface PublisherTypeProperty extends PublisherTypeAccessor, PublisherTypeMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given value (setter) as of
		 * {@link #setPublisherType(Class)} and returns the very same value
		 * (getter).
		 * 
		 * @param aPublisherType The value to set (via
		 *        {@link #setPublisherType(Class)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default Class<?> letPublisherType( Class<?> aPublisherType ) {
			setPublisherType( aPublisherType );
			return aPublisherType;
		}
	}
}
