// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.observer;

import org.refcodes.matcher.MatcherSchema;
import org.refcodes.schema.Schema;

/**
 * Matches by event publisher type (EVENT PUBLISHER TYPE).
 * 
 * @param <E> The matchee type.
 * @param <PT> The publisher type.
 */
public class PublisherTypeOfEventMatcher<E extends MetaDataEvent<?, ?>, PT extends Object> extends AbstractEventMatcher<E> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "PUBLISHER_TYPE_OF";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Class<? extends PT> _eventPublisherType;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new publisher is assignable from matcher impl.
	 *
	 * @param aEventPublisherType the event publisher type
	 */
	public PublisherTypeOfEventMatcher( Class<? extends PT> aEventPublisherType ) {
		super( ALIAS, "Matches by event publisher type (PUBLISHER TYPE OF)." );
		_eventPublisherType = aEventPublisherType;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( E aEvent ) {
		if ( _eventPublisherType != null ) {
			if ( !aEvent.getMetaData().getPublisherType().isAssignableFrom( _eventPublisherType ) ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatcherSchema toSchema() {
		final MatcherSchema theSchema = super.toSchema();
		theSchema.put( Schema.VALUE, _eventPublisherType );
		return theSchema;
	}
}