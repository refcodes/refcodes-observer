package org.refcodes.observer;

/**
 * An {@link SubscribeEvent} is published by an {@link Observable} when an
 * observer is being subscribed.
 *
 * An {@link SubscribeEvent} provides a observable being the origin of the
 * {@link SubscribeEvent}.
 * 
 * @param <SRC> The type of the observable.
 */
public class SubscribeEvent<SRC> extends AbstractEvent<SRC> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Instantiates a new subscribe event.
	 *
	 * @param aSource The according source (origin).
	 */
	public SubscribeEvent( SRC aSource ) {
		super( aSource );
	}
}
