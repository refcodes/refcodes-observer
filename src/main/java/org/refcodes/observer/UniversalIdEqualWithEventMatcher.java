// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.observer;

import org.refcodes.matcher.MatcherSchema;
import org.refcodes.schema.Schema;

/**
 * Matches the given universal ID with the universal ID stored in an event's
 * meta data (UNIVERSAL ID EQUAL WITH).
 * 
 * @param <E> THe matchee type.
 */
public class UniversalIdEqualWithEventMatcher<E extends MetaDataEvent<?, ?>> extends AbstractEventMatcher<E> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "UNIVERSAL_ID_EQUAL_WITH";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _universalId;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new Universal-ID equal with matcher impl.
	 *
	 * @param aUid the Universal-TID
	 */
	public UniversalIdEqualWithEventMatcher( String aUid ) {
		super( ALIAS, "Matches the given universal ID <" + aUid + "> with the universal ID stored in an event's meta data (UNIVERSAL ID EQUAL WITH)." );
		_universalId = aUid;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( E aEvent ) {
		if ( _universalId != null ) {
			return ( _universalId.equals( aEvent.getMetaData().getUniversalId() ) );
		}
		else if ( aEvent.getMetaData().getUniversalId() == null ) {
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatcherSchema toSchema() {
		final MatcherSchema theSchema = super.toSchema();
		theSchema.put( Schema.VALUE, _universalId );
		return theSchema;
	}
}