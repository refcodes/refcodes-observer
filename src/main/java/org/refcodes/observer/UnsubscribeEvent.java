package org.refcodes.observer;

/**
 * An {@link UnsubscribeEvent} is published by an {@link Observable} when an
 * observer is being unsubscribed.
 *
 * An {@link UnsubscribeEvent} provides a observable being the origin of the
 * {@link UnsubscribeEvent}.
 * 
 * @param <SRC> The type of the observable.
 */
public class UnsubscribeEvent<SRC> extends AbstractEvent<SRC> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new unsubscribe event.
	 *
	 * @param aSource The according source (origin).
	 */
	public UnsubscribeEvent( SRC aSource ) {
		super( aSource );
	}
}
