// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

import org.junit.jupiter.api.Test;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.runtime.SystemProperty;

/**
 * The Class ObservableTest.
 */
public class ObservableTest extends AbstractObservable<TestObserver, String> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String JOIN = ExecutionStrategy.JOIN.name();
	private static final String PARALLEL = ExecutionStrategy.PARALLEL.name();
	private static final String SEQUENTIAL = ExecutionStrategy.SEQUENTIAL.name();

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testObservable() throws VetoException {
		subscribeObserver( new TestEventListener1() );
		subscribeObserver( new TestEventListener2() );
		fireEvent( JOIN, ExecutionStrategy.JOIN );
		fireEvent( PARALLEL, ExecutionStrategy.PARALLEL );
		fireEvent( SEQUENTIAL, ExecutionStrategy.SEQUENTIAL );
	}

	@Override
	protected boolean fireEvent( String aEvent, TestObserver aEventListener, ExecutionStrategy aEventExecutionStrategy ) throws VetoException {
		if ( SEQUENTIAL.equals( aEvent ) ) {
			return aEventListener.pushSequentialEvent( aEvent );
		}
		if ( PARALLEL.equals( aEvent ) ) {
			aEventListener.pushConcurrentEvent( aEvent );
		}
		if ( JOIN.equals( aEvent ) ) {
			return aEventListener.pushJoinEvent( aEvent );
		}
		return true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class TestEventListener1 implements TestObserver {

		@Override
		public boolean pushSequentialEvent( String aEvent ) throws VetoException {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "TestEventListener1.pushSequentialEvent(): \"" + aEvent + "\"" );
			}
			return true;
		}

		@Override
		public void pushConcurrentEvent( String aEvent ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "TestEventListener1.pushConcurrentEvent(): \"" + aEvent + "\"" );
			}
		}

		@Override
		public boolean pushJoinEvent( String aEvent ) throws VetoException {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "TestEventListener1.pushJoinEvent(): \"" + aEvent + "\"" );
			}
			return true;
		}
	}

	private class TestEventListener2 implements TestObserver {

		@Override
		public boolean pushSequentialEvent( String aEvent ) throws VetoException {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "TestEventListener2.pushSequentialEvent(): \"" + aEvent + "\"" );
			}
			return true;
		}

		@Override
		public void pushConcurrentEvent( String aEvent ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "TestEventListener2.pushConcurrentEvent(): \"" + aEvent + "\"" );
			}
		}

		@Override
		public boolean pushJoinEvent( String aEvent ) throws VetoException {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "TestEventListener2.pushJoinEvent(): \"" + aEvent + "\"" );
			}
			return true;
		}
	}
}
