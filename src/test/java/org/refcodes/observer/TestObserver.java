// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.observer;

import org.refcodes.exception.VetoException;

/**
 * An asynchronous update interface for receiving notifications about Test
 * information as the Test is constructed.
 */
public interface TestObserver {

	/**
	 * This method is called when information about an Test which was previously
	 * requested using an asynchronous interface becomes available.
	 *
	 * @param aEvent the event
	 * 
	 * @return true, if push sequential event
	 * 
	 * @throws VetoException the veto exception
	 */
	boolean pushSequentialEvent( String aEvent ) throws VetoException;

	/**
	 * This method is called when information about an Test which was previously
	 * requested using an asynchronous interface becomes available.
	 *
	 * @param aEvent the event
	 */
	void pushConcurrentEvent( String aEvent );

	/**
	 * This method is called when information about an Test which was previously
	 * requested using an asynchronous interface becomes available.
	 *
	 * @param aEvent the event
	 * 
	 * @return true, if push join event
	 * 
	 * @throws VetoException the veto exception
	 */
	boolean pushJoinEvent( final String aEvent ) throws VetoException;

}
